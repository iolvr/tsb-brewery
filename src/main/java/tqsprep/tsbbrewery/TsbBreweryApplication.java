package tqsprep.tsbbrewery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TsbBreweryApplication {

    public static void main(String[] args) {
        SpringApplication.run(TsbBreweryApplication.class, args);
    }

}
